const imageList = document.querySelectorAll('.image-to-show');


function handleImageChange() {
    for (let i = 0; i < imageList.length; i++) {
        const isVisible = imageList[i].classList.contains('display');
        if (isVisible) {
            imageList[i].classList.remove('display');
            if (imageList[i + 1] === undefined) {
                imageList[0].classList.add('display');
            } else {
                imageList[i + 1].classList.add('display');
            }

            break;
        }
    }

}

let intervalId = setInterval(handleImageChange, 10000);

const startButton = document.querySelector('.btn-start');
startButton.addEventListener('click', () => {
    intervalId = setInterval(handleImageChange, 10000);
});

const stopButton = document.querySelector('.btn-stop');
stopButton.addEventListener('click', () => {
    clearInterval(intervalId);
});


