const keyButtons = document.querySelectorAll('.btn');

document.addEventListener('keydown',(event) => {
    const key = event.key;
    keyButtons.forEach((item) => {
        if (item.innerHTML.toUpperCase() === key.toUpperCase()) {
            item.style.backgroundColor = 'blue';
        } else {
            item.style.backgroundColor = '';
        }
    });
});

