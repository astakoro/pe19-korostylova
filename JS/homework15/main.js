function processUserInput() {
    let number;
    do {
        number = prompt("Введите число:");
        if (number === null) {
            return;
        }
    } while (!Number.isInteger(+number) || number.trim() === '');
    alert(factorial(number));
}

function factorial(n) {
    return n > 0 ? n * factorial(n - 1) : 1;
}


processUserInput();
