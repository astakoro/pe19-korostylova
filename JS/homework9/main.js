document.querySelector('.tabs').addEventListener('click', (event) => {
    const eventTarget = event.target;
    const itemList = document.querySelectorAll('.tabs li');
    for (let i = 0; i < itemList.length; i++) {
        itemList[i].classList.remove('active');
    }
    eventTarget.classList.add('active');

    const contentList = document.getElementsByClassName('blocks-content');
    for (let i = 0; i < contentList.length; i++) {
        contentList[i].classList.remove('first-block');
    }
    const contentElement = document.querySelector('.'+ eventTarget.innerHTML.toLowerCase());
    contentElement.classList.add('first-block')
});


