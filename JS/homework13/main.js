const buttonChange = document.querySelector('.local');
const button = document.querySelector('.btn1');
const changeTheme = () => {
    if (localStorage.getItem('theme') === 'white') {
        refreshColor();
    } else {
        backToStartTheme();
    }
};

buttonChange.addEventListener('click',changeTheme);
//
window.onload = () => {
    console.log(localStorage.getItem('theme'));
    if (localStorage.getItem('theme') === 'black') {
        refreshColor();
    } else {
        backToStartTheme();
    }
};

function refreshColor(){
    localStorage.setItem('theme', 'black');
    document.body.style.backgroundColor = 'black';
    button.style.backgroundColor = 'yellow';
    button.style.color = 'black';


}

function backToStartTheme() {
    localStorage.setItem('theme', 'white');
    document.body.style.backgroundColor = 'white';
    button.style.backgroundColor = '#16A085';
    button.style.color = 'white';
}

