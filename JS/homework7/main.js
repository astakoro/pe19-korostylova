function generateList (valueList) {
    return valueList.map((value) => {
        const element = document.createElement('li');
        element.innerText = value;
        return element;
    })
}

const myList = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
// console.log(generateList(myList));
const elementList = generateList(myList);

document.getElementById('list-items').append(...elementList);




