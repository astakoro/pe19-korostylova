const input = document.querySelector('input[type="number"]');
const spanWrapper = document.querySelector('.span-wrapper');
const closeIcon = document.createElement('span');
closeIcon.innerHTML = 'x';


input.addEventListener('focus', (event) => {
    input.classList.add('input-focused');
});
input.addEventListener('blur', (event) => {
    input.classList.remove('input-focused');
    const spanError = document.querySelector('.text-error');
    spanError.innerHTML = '';
    const inputValue = parseFloat(event.target.value);
    input.classList.remove('input-error');

    console.log(inputValue);
    if (inputValue > 0) {
        const span = document.createElement('span');
        span.innerHTML = `Текущая цена: ${inputValue}`;
        span.append(closeIcon);
        const spanWrapperContent = spanWrapper.querySelector('span');
        if (spanWrapperContent) {
            spanWrapperContent.remove();
        }
        spanWrapper.append(span);
    } else if (!Number.isNaN(inputValue)) {
        input.classList.add('input-error');
        spanError.innerHTML = `Please enter correct price`;

    }



});

closeIcon.addEventListener('click', (event) => {
    const spanWrapperContent = spanWrapper.querySelector('span');
    spanWrapperContent.remove();
    input.value = '';


});